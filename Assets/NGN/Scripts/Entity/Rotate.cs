﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class Rotate : ITick
    {
        readonly Transform transform;
        readonly IReadInput inputRotation;
        readonly RotateSettings rotateSettings;

        private float angle;
        private Vector3 rotation;

        public Rotate(Transform _transform, IReadInput _inputRotation, RotateSettings _rotateSettings)
        {
            transform = _transform;
            inputRotation = _inputRotation;
            rotateSettings = _rotateSettings;
        }

        public void Tick()
        {
            inputRotation.ReadInput();
            angle += inputRotation.InputPower * rotateSettings.RotateSpeed * 10 * Time.deltaTime;

            if (rotateSettings.MaxAngle < 360)
                angle = Mathf.Clamp(angle, -rotateSettings.MaxAngle, rotateSettings.MaxAngle);

            if (rotateSettings.Axis == RotateSettings.AxisType.X)
                rotation = new Vector3(angle, transform.eulerAngles.y, transform.eulerAngles.z);
            else if (rotateSettings.Axis == RotateSettings.AxisType.Y)
                rotation = new Vector3(transform.eulerAngles.x, angle, transform.eulerAngles.z);
            else if (rotateSettings.Axis == RotateSettings.AxisType.Z)
                rotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, angle);

            transform.eulerAngles = rotation;
        }
    }
}


