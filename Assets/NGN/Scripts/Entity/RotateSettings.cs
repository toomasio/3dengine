﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class RotateSettings
    {
        public enum AxisType { X, Y, Z}
        [SerializeField] private AxisType axis;
        public AxisType Axis { get { return axis; } }
        [SerializeField] private float rotateSpeed;
        public float RotateSpeed { get { return rotateSpeed; } }
        [SerializeField] private float maxAngle = 360;
        public float MaxAngle { get { return maxAngle; } }
    }
}


