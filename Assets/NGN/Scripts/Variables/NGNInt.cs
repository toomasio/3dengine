﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class NGNInt : NGNValue, IValue<int>, IValueNumeric<int>
    {
        public int Value { get ; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}


