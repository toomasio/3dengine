﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public interface IValueNumeric<T> : IValue<T>
    {
        T MinValue { get; }
        T MaxValue { get; }
    }
}

