﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public interface IReadInput
    {
        float InputPower { get; }
        void ReadInput();
    }
}

