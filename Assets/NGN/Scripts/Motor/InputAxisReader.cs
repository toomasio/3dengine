﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class InputAxisReader : IReadInput
    {
        [SerializeField] private InputProperty input;
        [SerializeField] private bool rawInput;
        public float InputPower { get; private set; }

        public void ReadInput()
        {
            InputPower = input.GetAxis(rawInput);
        }
    }
}


