﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [System.Serializable]
    public class NGNStateHandler : IStateHandler
    {
        [SerializeField] protected string stateName;
        public string StateName { get { return stateName; } }
        [SerializeField] protected int priority;
        public int Priority { get { return priority; } }
        public bool continuous;
        public NGNTransition finishedCondition;
        public enum StateCallType { OnEnter, OnUpdate, OnFixedUpdate, OnLateUpdate, OnFinished }
        public int stateCallMask;
        [SerializeField] protected NGNActionStack[] onEnterActions;
        [SerializeField] protected NGNActionStack[] onUpdateActions;
        [SerializeField] protected NGNActionStack[] onFixedUpdateActions;
        [SerializeField] protected NGNActionStack[] onLateUpdateActions;
        [SerializeField] protected NGNActionStack[] onFinishedActions;

        public bool IsActive { get; set; }

        private readonly List<T1Action> finishedCallbacks = new List<T1Action>();
        protected T1Action T1Cache = new T1Action();
        protected NGNEntityStateController owner;
        public void Initialize(NGNEntityStateController _owner)
        {
            owner = _owner;
            DoEnterActions();
            SubscribeToMono();
            if (!continuous && finishedCondition != null)
                finishedCondition.Initialize(owner, OnFinished);
        }

        void SubscribeToMono()
        {
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnUpdate)))
                DoUpdateActions(onUpdateActions, StateCallType.OnUpdate, true);
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnFixedUpdate)))
                DoUpdateActions(onFixedUpdateActions, StateCallType.OnFixedUpdate, true);
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnLateUpdate)))
                DoUpdateActions(onLateUpdateActions, StateCallType.OnLateUpdate, true);
        }

        void UnSubscribeToMono()
        {
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnUpdate)))
                DoUpdateActions(onUpdateActions, StateCallType.OnUpdate, false);
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnFixedUpdate)))
                DoUpdateActions(onFixedUpdateActions, StateCallType.OnFixedUpdate, false);
            if (stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnLateUpdate)))
                DoUpdateActions(onLateUpdateActions, StateCallType.OnLateUpdate, false);
        }

        void DoEnterActions()
        {
            if (!(stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnEnter))))
                return;

            for (int i = 0; i < onEnterActions.Length; i++)
            {
                onEnterActions[i].Initialize();
                onEnterActions[i].DoActions();
            }
        }

        void DoFinishedActions()
        {
            if (!(stateCallMask == (stateCallMask | (1 << (int)StateCallType.OnFinished))))
                return;

            for (int i = 0; i < onFinishedActions.Length; i++)
            {
                onFinishedActions[i].Initialize();
                onFinishedActions[i].DoActions();
            }
        }

        void DoUpdateActions(NGNActionStack[] _actions, StateCallType _callType, bool _subscribe)
        {
            for (int i = 0; i < _actions.Length; i++)
            {
                _actions[i].Initialize();

                if (_callType == StateCallType.OnUpdate)
                {
                    if (_subscribe)
                        NGNMonoHandler.SubscribeToUpdate(_actions[i].DoActions);
                    else
                        NGNMonoHandler.UnSubscribeToUpdate(_actions[i].DoActions);
                }
                else if (_callType == StateCallType.OnFixedUpdate)
                {
                    if (_subscribe)
                        NGNMonoHandler.SubscribeToFixedUpdate(_actions[i].DoActions);
                    else
                        NGNMonoHandler.UnSubscribeToFixedUpdate(_actions[i].DoActions);
                }
                else if (_callType == StateCallType.OnFixedUpdate)
                {
                    if (_subscribe)
                        NGNMonoHandler.SubscribeToLateUpdate(_actions[i].DoActions);
                    else
                        NGNMonoHandler.UnSubscribeToLateUpdate(_actions[i].DoActions);
                }

            }
        }

        void OnFinished()
        {
            DoFinishedActions();
            UnSubscribeToMono();
            if (finishedCondition != null)
                finishedCondition.UnSubscribe(OnFinished);

            for (int i = 0; i < finishedCallbacks.Count; i++)
            {
                var obj = finishedCallbacks[i].objectValue;
                var action = finishedCallbacks[i].action;
                action.Invoke(obj);
            }
            IsActive = false;
        }

        public void Subscribe<T>(T _object, System.Action<T> _onFinishedCallback) where T : class
        {
            T1Cache.objectValue = _object as T;
            T1Cache.action = arg => _onFinishedCallback(arg as T);
            finishedCallbacks.Add(T1Cache.Copy());
        }

        public void UnSubscribe<T>(T _object) where T : class
        {
            T1Cache.Clear();
            T1Cache = GetMatch(_object, out int ind);
            if (!T1Cache.IsEmpty)
                finishedCallbacks.RemoveAt(ind);

            //unsub from condition as well
            finishedCondition.UnSubscribe(OnFinished);
        }

        T1Action GetMatch(object _T1Value, out int _ind)
        {
            _ind = -1;
            for (int i = 0; i < finishedCallbacks.Count; i++)
            {
                if (finishedCallbacks[i].objectValue == _T1Value)
                {
                    _ind = i;
                    return finishedCallbacks[i];
                }

            }
            return default;
        }

    }
}


