﻿
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.Globalization;

namespace NGN
{
    /// <summary>
    /// Extends how ScriptableObject object references are displayed in the inspector
    /// Shows you all values under the object reference
    /// Also provides a button to create a new ScriptableObject if property is null.
    /// </summary>
    [CustomPropertyDrawer(typeof(NGNAction), true)]
    public class NGNActionPropertyDrawer : NGNScriptableObjectPropertyDrawer
    {
        protected SerializedProperty owner;
        
        protected override void GetProperties(SerializedObject ser)
        {
            base.GetProperties(ser);
            owner = ser.FindProperty("owner");
            //Debug.Log(owner.objectReferenceValue.name);
        }

    }
}


