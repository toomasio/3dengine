﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
namespace NGN
{

    public class NGNEntityStateController : NGNEntity
    {
        [SerializeField] protected NGNValue[] variables;
        [SerializeField] protected NGNStateHandler[] states;
        [SerializeField] protected NGNStateTransitionHandler[] transitions;

        protected NGNStateHandler preferredState;
        protected List<NGNStateHandler> currentActiveStates = new List<NGNStateHandler>();

        protected void Awake()
        {
            OnBeginTransitions();
            OnBeginStates();
        }

        protected virtual void OnBeginTransitions()
        {
            for (int i = 0; i < transitions.Length; i++)
            {
                transitions[i].Initialize(this);
            }
        }

        protected virtual void OnBeginStates()
        {
            AddActiveState(0);
        }

        public virtual void AddActiveState(int _stateInd)
        {
            currentActiveStates.Add(states[_stateInd]);
            preferredState = GetPreferredState();
            BeginPreferredState();
        }

        protected virtual void BeginPreferredState()
        {
            preferredState.Subscribe(preferredState, OnStateFinished);
            preferredState.Initialize(this);
        }

        protected virtual void OnStateFinished(NGNStateHandler _finishedState)
        {
            currentActiveStates.Remove(_finishedState);
            preferredState = GetPreferredState();
            _finishedState.UnSubscribe(_finishedState);
            BeginPreferredState();
        }

        protected virtual NGNStateHandler GetPreferredState()
        {
            var curState = currentActiveStates[0];
            for (int i = 0; i < currentActiveStates.Count; i++)
            {
                var state = currentActiveStates[i];
                if (curState.Priority < state.Priority)
                    curState = state;
            }
            return curState;
        }

        protected virtual void OnDestroy()
        {
        }

        public string[] GetStateNames()
        {
            var names = new string[states.Length];
            for (int i = 0; i < states.Length; i++)
            {
                names[i] = states[i].StateName;
            }
            return names;
        }

    }
}


