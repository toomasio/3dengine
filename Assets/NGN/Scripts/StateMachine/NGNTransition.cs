﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    public class NGNTransition : NGNScriptableObject, ITransition
    {
        public bool IsTriggered { get; set; }
        [SerializeField] [HideInInspector] protected NGNEntityStateController owner;
        public NGNEntityStateController Owner { get { return owner; } set { owner = value; } }
        protected List<System.Action> onTransitionCallbacks = new List<System.Action>();

        public virtual void Initialize(NGNEntityStateController _owner)
        {
            owner = _owner;
            OnInitialize();
        }

        public virtual void Initialize(NGNEntityStateController _owner, System.Action _onTransitionCallback)
        {
            Subscribe(_onTransitionCallback);
            Initialize(_owner);
        }

        protected virtual void OnInitialize() { }

        protected virtual void OnTransition()
        {
            DoTransitionActions();
        }

        public virtual void Subscribe(System.Action _onTransitionCallback)
        {
            if (!onTransitionCallbacks.Contains(_onTransitionCallback))
                onTransitionCallbacks.Add(_onTransitionCallback);
        }

        public virtual void UnSubscribe(System.Action _onTransitionCallback)
        {
            if (onTransitionCallbacks.Contains(_onTransitionCallback))
                onTransitionCallbacks.Remove(_onTransitionCallback);
        }

        protected virtual void DoTransitionActions()
        {
            for (int i = 0; i < onTransitionCallbacks.Count; i++)
            {
                onTransitionCallbacks[i].Invoke();
            }
        }
    }
}


