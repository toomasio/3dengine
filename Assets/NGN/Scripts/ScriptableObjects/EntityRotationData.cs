﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NGN
{
    [CreateAssetMenu(menuName = "NGN/Data/Entity/RotationData", order = 1)]
    public class EntityRotationData : NGNScriptableObject
    {
        public enum AxisType { X, Y, Z }
        [SerializeField] private AxisType axis;
        public AxisType Axis { get { return axis; } }
        [SerializeField] private float rotationSpeed = 5;
        public float RotationSpeed { get { return rotationSpeed; } }
        [SerializeField] private float maxAngle = 360;
        public float MaxAngle { get { return maxAngle; } }
    }
}


