﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using System.Reflection;

public static class UtilitiesType
{

    public static bool IsList(this Type _type)
    {
        return _type.GetTypeInfo().IsGenericType && _type.GetGenericTypeDefinition() == typeof(List<>);
    }

    public static bool IsList(this object _obj)
    {
        var type = _obj.GetType();
        return type.IsList();
    }

    public static string[] GetAllOfInterfaceNames(this Type interfaceType)
    {
        return GetAllOfInterface(interfaceType)
            .Select(x => x.Name)
            .ToArray();
    }

    public static Type[] GetAllOfInterface(this Type interfaceType)
    {
        return AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(x => interfaceType.IsAssignableFrom(x) && x != interfaceType && !x.IsAbstract)
            .OrderBy(x => x.Name)
            .ToArray();
    }

    public static string[] GetAllSubClassesNames(this Type type)
    {
        return GetAllSubClasses(type)
            .Select(x => x.Name)
            .ToArray();
    }

    public static Type[] GetAllSubClasses(this Type type)
    {
        return AppDomain.CurrentDomain.GetAssemblies()
            .SelectMany(s => s.GetTypes())
            .Where(x => x.IsSubclassOf(type) && !x.IsAbstract)
            .OrderBy(x => x.Name)
            .ToArray();
    }
}
